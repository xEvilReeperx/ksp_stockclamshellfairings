﻿using UnityEngine;

namespace StockClamshellFairings
{
// ReSharper disable once UnusedMember.Global
    public class DebuggingMethods
    {
// ReSharper disable once UnusedMember.Local
        public static void DebugPoint(Vector3 point, Transform anchor)
        {
            var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            PartLoader.StripComponent<Collider>(go);

            go.transform.localScale = Vector3.one * 0.1f;
            go.transform.position = point;
            go.transform.parent = anchor;

            go.renderer.material = new Material(Shader.Find("Unlit/Transparent")) { color = Color.white };
        }



// ReSharper disable once UnusedMember.Global
        public static void DebugDirection(Vector3 radialDirectionWorld, Transform anchor, Color[] colors = null)
        {
            if (colors == null)
                colors = new[]
                {
                    Color.white,
                    Color.red
                };

            var go = new GameObject("DebugRadial");
            var lr = go.AddComponent<LineRenderer>();
            lr.useWorldSpace = false;
            lr.SetColors(colors[0], colors[1]);
            lr.SetWidth(.6f, 0.12f);
            lr.SetPosition(0, anchor.transform.position);
            lr.SetPosition(1, radialDirectionWorld * 10f + anchor.transform.position);
            lr.transform.parent = anchor;
            lr.material = new Material(Shader.Find("Particles/Additive")) {color = Color.red};
        }
    }
}
