﻿using System;
using UnityEngine;

namespace StockClamshellFairings
{
    public class RadialVectorQuery
    {
        // in local space of ModuleProceduralFairing, not normalized
        public Vector3 Get(ModuleProceduralFairing fairingModule, Vector3 worldPoint)
        {
            if (fairingModule == null) throw new ArgumentNullException("fairingModule");

            var panelLocal = fairingModule.transform.InverseTransformPoint(worldPoint);

            return Vector3.ProjectOnPlane(panelLocal - fairingModule.axis, fairingModule.axis);
        }
    }
}
