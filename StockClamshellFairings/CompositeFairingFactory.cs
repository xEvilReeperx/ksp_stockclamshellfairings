﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProceduralFairings;
using UnityEngine;
using Object = UnityEngine.Object;

namespace StockClamshellFairings
{
    public class CompositeFairingFactory
    {
        private readonly RadialVectorQuery _radialVectorQuery;

        public CompositeFairingFactory(RadialVectorQuery radialVectorQuery)
        {
            if (radialVectorQuery == null) throw new ArgumentNullException("radialVectorQuery");
            _radialVectorQuery = radialVectorQuery;
        }


        public void Create(VerticalFairingGrouping grouping)
        {
            if (grouping == null) throw new ArgumentNullException("grouping");

            if (!grouping.Panels.Any()) return;

            var composite = new GameObject("FairingComposite");

            SetupCompositePhysics(composite, grouping);

            foreach (var panel in grouping.Panels)
                AddPanelToCompositeFairing(composite, panel);

        }


        private void SetupCompositePhysics(GameObject composite, VerticalFairingGrouping grouping)
        {
            if (composite == null) throw new ArgumentNullException("composite");
            if (grouping == null) throw new ArgumentNullException("grouping");

            var panels = grouping.Panels.ToList();

            composite.transform.position = panels.First().go.transform.position;
            composite.transform.rotation = Quaternion.identity;

            composite.AddComponent<physicalObject>();
            var rb = composite.AddComponent<Rigidbody>();
            rb.useGravity = false;
            rb.mass = panels.Sum(fp => fp.go.rigidbody.mass);
            rb.drag = panels.Average(fp => fp.go.rigidbody.drag);
            rb.angularDrag = panels.Average(fp => fp.go.rigidbody.angularDrag);
            rb.centerOfMass = composite.transform.InverseTransformPoint(CalculateCoM(panels));
            rb.velocity = CalculatePointVelocity(grouping.Module, rb.worldCenterOfMass);

            var requiredForce = CalculateEjectionForce(grouping.Module, grouping.Panels.Max(fp => fp.go.rigidbody.mass),
                rb);

            // original
            rb.AddForce(grouping.Module.transform.TransformDirection(grouping.RadialDirection) * requiredForce, ForceMode.Force);
        }


        private static void AddPanelToCompositeFairing(GameObject composite, FairingPanel panel)
        {
            if (composite == null) throw new ArgumentNullException("composite");
            if (panel == null) throw new ArgumentNullException("panel");

            var child = new GameObject("FairingCompositeChild");

            child.AddComponent<MeshFilter>().sharedMesh = panel.go.GetComponent<MeshFilter>().sharedMesh;
            child.layer = panel.go.layer;

            var mr = child.AddComponent<MeshRenderer>();
            var copyFrom = panel.go.GetComponent<MeshRenderer>();

            mr.sharedMaterial = copyFrom.sharedMaterial;
            mr.receiveShadows = copyFrom.receiveShadows;

            child.transform.parent = composite.transform;
            child.transform.position = panel.go.transform.position;
            child.transform.rotation = panel.go.transform.rotation;

            // now we'll steal all child transforms (part and terrain colliders) since the original doesn't
            // need them anymore and it's easier than copying
            while (panel.go.transform.childCount > 0)
                panel.go.transform.GetChild(0).parent = child.transform;

            // why destroy the original ones rather than absorb them entirely? we can't destroy
            // their physicalObject components cleanly without spewing a whole bunch of NREs
            // from FlightIntegrator, so stealing their meshes, materials and colliders ends up
            // being the way to go
            panel.go.SetActive(false);
            Object.Destroy(panel.go);
        }


        private static Vector3 CalculateCoM(IEnumerable<FairingPanel> panels)
        {
            if (panels == null) throw new ArgumentNullException("panels");

            var com = Vector3.zero;
            float mass = 0f;

            foreach (var p in panels)
            {
                com += p.go.rigidbody.worldCenterOfMass * p.go.rigidbody.mass;
                mass += p.go.rigidbody.mass;
            }

            if (mass > 0f)
                return com / mass;
            return com;
        }


        private Vector3 CalculatePointVelocity(ModuleProceduralFairing fairingModule, Vector3 compositeWorldPosition)
        {
            if (fairingModule == null) throw new ArgumentNullException("fairingModule");

            // local space radial direction of composite from center axis line of fairing module
            var radial = _radialVectorQuery.Get(fairingModule, compositeWorldPosition);

            return fairingModule.rigidbody.GetPointVelocity(fairingModule.transform.position + radial);
        }


        private static float CalculateEjectionForce(ModuleProceduralFairing fairingModule, float panelMass, Rigidbody composite)
        {
            // F = m * (v/t)
            // calculate how fast the panel would go if we applied the specified ejectionForce to it
            var targetVelocity = fairingModule.ejectionForce / panelMass * Time.fixedDeltaTime;

            // calculate a force that would cause an equivalent delta velocity change in the composite rigidbody
            return composite.mass * (targetVelocity / Time.fixedDeltaTime);;
        }
    }
}
