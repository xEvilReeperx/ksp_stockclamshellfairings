﻿using System.Linq;
using System.Reflection;
using UnityEngine;

namespace StockClamshellFairings
{
    [KSPAddon(KSPAddon.Startup.MainMenu, true)]
    public class ModuleInstaller : MonoBehaviour
    {
        private void Awake()
        {
            var fairingPrefabs = PartLoader.LoadedPartsList
                .Where(ap => ap.partPrefab.gameObject.GetComponent<ModuleProceduralFairing>() != null)
                .Where(ap => ap.partPrefab.GetComponent<ModuleClamshellFairings>() == null)
                .Select(ap => ap.partPrefab)
                .ToList();

            foreach (var fairingPart in fairingPrefabs)
            {
                print("Installing ModuleClamshellFairings on " + fairingPart.name);

                var pm = fairingPart.gameObject.AddComponent<ModuleClamshellFairings>();

                typeof (PartModule).InvokeMember("Awake", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, pm,
                    new object[] {});
            }

        }
    }
}
