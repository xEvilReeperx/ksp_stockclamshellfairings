﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProceduralFairings;
using UnityEngine;

namespace StockClamshellFairings
{
    using GroupingDictionary = Dictionary<Vector3, List<FairingPanel>>;
 
    public class FairingGroupingFactory
    {
        private readonly ModuleProceduralFairing _fairingModule;
        private readonly RadialVectorQuery _radialVectorQuery;
        private const float Epsilon = 0.001f;

        public FairingGroupingFactory(ModuleProceduralFairing fairingModule, RadialVectorQuery radialVectorQuery)
        {
            if (fairingModule == null) throw new ArgumentNullException("fairingModule");
            if (radialVectorQuery == null) throw new ArgumentNullException("radialVectorQuery");
            _fairingModule = fairingModule;
            _radialVectorQuery = radialVectorQuery;
        }


        public IEnumerable<VerticalFairingGrouping> CreateGroupings()
        {
            var groupings = new GroupingDictionary();

            foreach (var panel in _fairingModule.Panels)
                InsertPanelIntoGroup(groupings, panel, CalculateRadialNormalVector(panel));

            return groupings.Values.Select(fpl => new VerticalFairingGrouping(_fairingModule, CalculateRadialNormalVector(fpl.First()), fpl));
        }


        private Vector3 CalculateRadialNormalVector(FairingPanel panel)
        {
            return _radialVectorQuery.Get(_fairingModule, panel.go.transform.position).normalized;
        }


        private void InsertPanelIntoGroup(
            GroupingDictionary dictionary, 
            FairingPanel panel,
            Vector3 panelRadialDirection)
        {
            var group = FindGroup(dictionary, panelRadialDirection);

            if (group == null) // no group for this radial direction exists yet
            {
                dictionary.Add(panelRadialDirection, new List<FairingPanel> {panel});
            }
            else group.Add(panel);
        }


        private static List<FairingPanel> FindGroup(GroupingDictionary dictionary, Vector3 targetRadial)
        {
            Func<Vector3, bool> match = dirGroup => DirectionsMatch(targetRadial, dirGroup);

            return dictionary.Keys.Any(match) ? dictionary[dictionary.Keys.First(match)] : null;
        }


        private static bool DirectionsMatch(Vector3 first, Vector3 second)
        {
            // scalar_product(v1,v2)/(length(v1)*length(v2)) > 1 - epsilon
            return Vector3.Dot(first, second) / (first.magnitude * second.magnitude) > 1f - Epsilon;
        }
    }
}
