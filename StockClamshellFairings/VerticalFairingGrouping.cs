﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProceduralFairings;
using UnityEngine;

namespace StockClamshellFairings
{
    public class VerticalFairingGrouping
    {
        private readonly ModuleProceduralFairing _fairingModule;
        private readonly Vector3 _radialDirection;
        private readonly IEnumerable<FairingPanel> _panels;

        public VerticalFairingGrouping(ModuleProceduralFairing fairingModule, Vector3 radialDirection,
            IEnumerable<FairingPanel> panels)
        {
            if (fairingModule == null) throw new ArgumentNullException("fairingModule");
            if (panels == null) throw new ArgumentNullException("panels");
            if (radialDirection.IsZero()) throw new ArgumentException("radialDirection cannot be zero");

            _fairingModule = fairingModule;
            _radialDirection = radialDirection;
            _panels = panels;
        }


        public IEnumerable<FairingPanel> Panels
        {
            get { return _panels.Where(p => p != null); }
        }

        public Vector3 RadialDirection
        {
            get { return _radialDirection; }
        }

        public ModuleProceduralFairing Module
        {
            get { return _fairingModule; }
        }
    }
}
