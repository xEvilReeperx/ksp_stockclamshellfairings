﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace StockClamshellFairings
{

// ReSharper disable once UnusedMember.Global
    public class ModuleClamshellFairings : PartModule
    {
        private ModuleProceduralFairing _fairingModule;
        private IEnumerable<VerticalFairingGrouping> _verticalGroups = Enumerable.Empty<VerticalFairingGrouping>();


        [KSPField(guiActive = true, guiActiveEditor = true, guiName = "Style", isPersistant = true),
         UI_Toggle(affectSymCounterparts = UI_Scene.Editor, scene = UI_Scene.All,
             enabledText = "Clamshell", disabledText = "Confetti")] 
// ReSharper disable once MemberCanBePrivate.Global
        public bool
// ReSharper disable once InconsistentNaming
// ReSharper disable once ConvertToConstant.Global
// ReSharper disable once FieldCanBeMadeReadOnly.Global
            isClamshellDeployment = true;


        public override void OnStart(StartState state)
        {
            base.OnStart(state);
            if (state == StartState.Editor) return;

            _fairingModule = GetComponent<ModuleProceduralFairing>();

            if (_fairingModule == null)
            {
                Debug.LogWarning("Couldn't find ModuleProceduralFairing on " + part.partInfo.name);
                enabled = false;
                return;
            }

            _fairingModule.OnMoving.Add(OnFairingsJettisoned);
        }


// ReSharper disable once UnusedMember.Local
        private void OnDestroy()
        {
            if (_fairingModule != null)
                _fairingModule.OnMoving.Remove(OnFairingsJettisoned);
        }


        private void OnFairingsJettisoned(float f1, float f2)
        {
            if (!isClamshellDeployment) return;

            _verticalGroups = new FairingGroupingFactory(_fairingModule, new RadialVectorQuery()).CreateGroupings();

            // OnMoving is apparently called just before the fairings actually become individual objects
            // so we'll need to wait a little bit
            StartCoroutine(ScheduleFairingBind());
        }


        private IEnumerator ScheduleFairingBind()
        {
            yield return new WaitForEndOfFrame(); // wait until the fairings are individual objects

            CreateCompositeFairings();
        }


        private void CreateCompositeFairings()
        {
            var compositeFactory = new CompositeFairingFactory(new RadialVectorQuery());

            foreach (var vg in _verticalGroups)
                compositeFactory.Create(vg);
        }
    }
}
